module.exports = {
  pluginOptions: {
    autoRouting: {
      chunkNamePrefix: "page-"
    },
    cordovaPath: "src-cordova",
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: true
    }
  },

  devServer:{
    port: 8080,
    host: '0.0.0.0'
  },

  publicPath: "",
  // publicPath: process.env.NODE_ENV === "production" ? "/emmanueltv-app/" : "/",
  outputDir: "./src-cordova/www",

  pwa: {
    name: "Emmanuel TV",
    themeColor: "#0a3c70",
    msTileColor: "#0a3c70",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",

    // configure the workbox plugin
    workboxPluginMode: "GenerateSW",
    iconPaths: {
      favicon32: "img/icons/favicon-32x32.png",
      favicon16: "img/icons/favicon-16x16.png",
      appleTouchIcon: "img/icons/apple-touch-icon-152x152.png",
      maskIcon: "img/icons/safari-pinned-tab.svg",
      msTileImage: "img/icons/msapplication-icon-144x144.png"
    }
  }
};
