import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    error: false,
    isPageLanguageAvailable: true,
    title: "Emmanuel TV",
    loading: false,
    locale: window.localStorage.getItem("locale") || "en"
  },
  mutations: {},
  actions: {},
  modules: {}
});
