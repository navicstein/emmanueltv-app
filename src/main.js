import Vue from "vue";
import "./plugins/axios";
import "./plugins/socket";
import "./plugins/misc";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";
import "./registerServiceWorker";
import i18n from "./i18n";

// import i18n from "./i18n";

Vue.config.productionTip = true;

// import { ContentLoader } from "vue-content-loader";
Vue.mixin({
  components: {
    // ContentLoader
  },
  methods: {
    getLocale(who) {
      let locale = this.$i18n.locale;
      if (who === "fb") {
        let fbLocales = [
          "es_ES",
          "en_US",
          "fr_FR",
          "tr_TR",
          "sv_SE", // prefered codes are moved to line
          "af_ZA",
          "sq_AL",
          "ar_AR",
          "hy_AM",
          "ay_BO",
          "az_AZ",
          "eu_ES",
          "be_BY",
          "bn_IN",
          "bs_BA",
          "bg_BG",
          "ca_ES",
          "ck_US",
          "hr_HR",
          "cs_CZ",
          "da_DK",
          "nl_NL",
          "nl_BE",
          "en_PI",
          "en_GB",
          "en_UD",
          "eo_EO",
          "et_EE",
          "fo_FO",
          "tl_PH",
          "fi_FI",
          "fb_FI",
          "fr_CA",
          "gl_ES",
          "ka_GE",
          "de_DE",
          "el_GR",
          "gn_PY",
          "gu_IN",
          "he_IL",
          "hi_IN",
          "hu_HU",
          "is_IS",
          "id_ID",
          "ga_IE",
          "it_IT",
          "ja_JP",
          "jv_ID",
          "kn_IN",
          "kk_KZ",
          "km_KH",
          "tl_ST",
          "ko_KR",
          "ku_TR",
          "la_VA",
          "lv_LV",
          "fb_LT",
          "li_NL",
          "lt_LT",
          "mk_MK",
          "mg_MG",
          "ms_MY",
          "ml_IN",
          "mt_MT",
          "mr_IN",
          "mn_MN",
          "ne_NP",
          "se_NO",
          "nb_NO",
          "nn_NO",
          "ps_AF",
          "fa_IR",
          "pl_PL",
          "pt_BR",
          "pt_PT",
          "pa_IN",
          "qu_PE",
          "ro_RO",
          "rm_CH",
          "ru_RU",
          "sa_IN",
          "sr_RS",
          "zh_CN",
          "sk_SK",
          "sl_SI",
          "so_SO",
          "es_LA",
          "es_CL",
          "es_CO",
          "es_MX",
          "es_VE",
          "sw_KE",
          "sy_SY",
          "tg_TJ",
          "ta_IN",
          "tt_RU",
          "te_IN",
          "th_TH",
          "zh_HK",
          "zh_TW",
          "uk_UA",
          "ur_PK",
          "uz_UZ",
          "vi_VN",
          "cy_GB",
          "xh_ZA",
          "yi_DE",
          "zu_ZA"
        ];
        return fbLocales.find(x => x.match(locale)) || "en_US";
      }
    },
    setLocale(locale) {
      this.$i18n.locale = locale;
      console.log(`Set default locale to ->`, this.$i18n.locale);
      window.localStorage.setItem("locale", locale);
      // this.$router.go();
    }
  },
  created() {
    var title = this.$options.title;
    if (title) {
      store.state.title = title;
    }
  }
});

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount("#app");
