import Vue from "vue";
import Router from "vue-router";
import routes from "vue-auto-routing";
import { createRouterLayout } from "vue-router-layout";

Vue.use(Router);

const RouterLayout = createRouterLayout(layout => {
  return import("@/layouts/" + layout + ".vue");
});

let router = new Router({
  routes: [
    {
      path: "/",
      component: RouterLayout,
      children: routes
    }
  ],
  scrollBehavior(_to, _from, _savedPosition) {
    return { x: 0, y: 0 };
  }
});

router.beforeEach((_to, _from, next) => {
  navigator.vibrate([50]);
  next();
});

export default router;
